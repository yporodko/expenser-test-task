class ExpensesItem
	constructor: (options) ->
		{@name, @amount} = options

	setName: (name) ->
		@name = name

	getName: () ->
		@name

	setAmount: (amount) ->
		@amount = amount

	getAmount: () ->
		@amount


class ExpensesList
	constructor: (options) ->
		@list = [
			new ExpensesItem name: "Dinner", amount: 2350
			new ExpensesItem name: "Lunch", amount: 1799
			new ExpensesItem name: "Space Hulk", amount: 1299
		]

		@title = 'Expenses'

	addItem: (expensesItem) ->
		@list.push expensesItem

class ExpensesListView
	constructor: (options) ->
		{@templateSelector, @containerSelector, @list} = options

	render: () ->
		listComponent = $(@templateSelector).clone()

		console.log 'list redered'

		$(@containerSelector).append listComponent


expensesListView = new ExpensesListView templateSelector: '#sampleList', containerSelector: '#lists', list: new ExpensesList

expensesListView.render()