'use strict';


var ListsModel = require('../../models/lists');


module.exports = function (router) {

    var model = new ListsModel();


    router.get('/', function (req, res) {
        
        res.render('lists/index', model);
        
    });

};
