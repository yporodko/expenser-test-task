var COMPARATORS = {
	'amount-asc': function (item1, item2) {
		if(item1.getAmount() < item2.getAmount()) return -1;
	    if(item1.getAmount() > item2.getAmount()) return 1;
	    return 0;
	},
	'amount-desc': function (item1, item2) {
		if(item1.getAmount() < item2.getAmount()) return 1;
	    if(item2.getAmount() > item2.getAmount()) return -1;
	    return 0;
	},
	'name-asc': function (item1, item2) {
		if(item1.getName() < item2.getName()) return -1;
	    if(item1.getName() > item2.getName()) return 1;
	    return 0;
	},
	'name-desc': function (item1, item2) {
		if(item1.getName() < item2.getName()) return 1;
	    if(item1.getName() > item2.getName()) return -1;
	    return 0;
	}
}

var ExpensesItem = function (options) { 
	options = options || {};

	if (!options.list || !(options.list instanceof ExpensesList)) {
		console.error('ExpensesItem requires "list" to what it belongs.', options);
	}

	var _list = options.list || {};
	var _id = options.id || _list.getId() + '_' + Math.round(Math.random() * 1000000000);
	var _name = options.name || '';
	var _amount = options.amount || '';
	var _$this = $(this);

	this.setName = function(name) {
		_name = name;
		this.trigger('change', {type: 'name'});
	}

	this.setProps = function (props) {
		if (!props) return;

		if (props.name) _name = props.name;
		if (props.amount) _amount = props.amount;

		this.trigger('change', {type: 'props'});
	}

	this.getName = function () {
		return _name;
	}

	this.setAmount = function(amount) {
		_amount = amount;
		this.trigger('change', {type: 'amount'});
	}

	this.getAmount = function () {
		return _amount;
	}
	this.getId = function () {
		return _id;
	}
	this.remove = function () {
		_list.removeItem(this);
	}
	this.on = function(event, callback) {
      _$this.on(event, callback);
    }
    this.trigger = function (event, data) {
      _$this.trigger(event, data);
    }
}

var ExpensesList = function (options) {
	options = options || {};

	var _list = options.items || [
		// {name: "Dinner", amount: 2350},
		// {name: "Lunch", amount: 1799},
		// {name: "Space Hulk", amount: 1299},
	];
	var _title = options.title || 'Expenses';
	var _id = options.id || Math.round(Math.random() * 1000000000);
	var _currentComparator = options.comparator || 'name-asc';
	var _$this = $(this);
	var self = this;

	this.getId = function () {
		return _id;
	}
	this.setTitle = function (title) {
		_title = title;
		this.trigger('change', {type: 'title'});
	}
	this.getTitle = function () {
		return _title;
	}

	this.addItem = function (expensesItem) {

		if (expensesItem instanceof Array) {
			var addedItems = [];
			$.each(expensesItem, function (i, item) {
				if (!(item instanceof ExpensesItem)) {
					$.extend(item, {list: self});

					item = new ExpensesItem(item); 
				}

				_list.push(item);
				addedItems.push(item);
			});
			
			expensesItem = addedItems;
		} else {
			if (!(expensesItem instanceof ExpensesItem)) {
				$.extend(expensesItem, {list: self});
				expensesItem = new ExpensesItem(expensesItem); 

				expensesItem.on('change', function (e) {
					self.trigger('change', {type: 'item', data: e.data});
				});
			}
			
			_list.push(expensesItem);
		}

		this.trigger('add', expensesItem);
		this.trigger('change', {type: 'addItem', data: expensesItem});
	}

	this.getItems = function () {
		return _list;
	}
	this.removeItem = function (item) {
		if (!item)
			return;

		var indexToRemove = -1;

		if (_list.indexOf) {
			var indexToRemove = _list.indexOf(item);	
		} else {
			var idToRemove = item;

			if (item instanceof ExpensesItem) {			
				idToRemove = item.getId();
			}

			for (var i = 0; i < _list.length; i++) {
				var listItem = _list[i];

				if (listItem.getId() === idToRemove) {
					indexToRemove = i;
					break;
				}
			}
		}

		var removedItem = _list.splice(indexToRemove, 1);
		self.trigger('remove', removedItem);
		self.trigger('change', {type: 'remove', data: removedItem});
	}

	this.remove = function () {
		self.trigger('remove', this);
	}

	this.getTotal = function () {
		var sum = 0;

		for (var i in _list) {
			var item = _list[i];
			sum += item.getAmount();
		}
		return sum;
	}
	this.sort = function (comparator, options) {
		options = options || {};

		if (typeof comparator === 'function') {
			_list.sort(comparator);

			if (!options.silent)
				this.trigger('change');
		}
	}

	this.setComparator = function (comparatorName, options) {
		var comparator = COMPARATORS[comparatorName];
		_currentComparator = comparatorName;

		this.sort(comparator, options);
	}
	this.getCurrentComparator = function () {
		return _currentComparator;
	}

	this.on = function(event, callback) {
      _$this.on(event, callback);
    }
    this.trigger = function (event, data) {
      _$this.trigger(event, data);
    }

    this.serialize = function () {
    	var items = [];
    	$.each(_list, function (i, item) {
    		items.push({
    			id: item.getId(),
    			name: item.getName(),
    			amount: item.getAmount(),
    		});
    	});
    	return {
    		id: _id,
    		title: _title,
    		items: items,
    		comparator: _currentComparator,
    	}
    }

    this.size = function () {
    	return _list.length;
    }

    var checkedList = [];
	$.each(_list, function (i, item) {
		if (item instanceof ExpensesItem) {
			checkedList.push(item);
		} else {
			$.extend(item, {list: self});
			var expensesItem = new ExpensesItem(item);
			expensesItem.on('change', function (e) {
				self.trigger('change', {type: 'item', data: e.data});
			});

			checkedList.push(expensesItem);
		}
	});
	_list = checkedList;
}

var _itemTemplate = $('<div class="item row"> \
						<div class="col-xs-6 name"> \
							<span class="value"></span> \
							<span class="actions"> \
								<i class="fa fa-pencil edit"></i> \
								<i class="fa fa-times delete"></i> \
							</span> \
						</div> \
						<div class="col-xs-6 amount"> \
							<span class="dollar-sign">$</span> \
							<span class="value">0.00</span> \
						</div> \
					</div>');

var itemEditTemplate = $('<form class="col-xs-12 form-inline" role="form"> \
		<div class="row"> \
		<div class="form-group name col-xs-6"> \
			<input type="text" class="form-control" maxlength="30" name="name" placeholder="Name"> \
		</div> \
		<div class="form-group amount col-xs-6"> \
			<input type="text" class="form-control" maxlength="25" name="amount" placeholder="amount"> \
			<div class="actions"> \
				<button class="btn btn-default save"> \
					<i class="fa fa-check-circle-o"></i> \
				</button><button class="btn btn-default cancel"> \
					<i class="fa fa-times-circle-o"></i> \
				</button> \
			</div> \
		</div> \
		</div> \
	</form>');

var titleEditTemplate = $('<form class="form-inline" role="form"> \
		<div class="form-group title"> \
			<input type="text" class="form-control" maxlength="45" name="title" placeholder="title"> \
			<div class="actions"> \
				<button class="btn btn-default save"> \
					<i class="fa fa-check-circle-o"></i> \
				</button><button class="btn btn-default cancel"> \
					<i class="fa fa-times-circle-o"></i> \
				</button> \
			</div> \
		</div> \
	</form>');

var renderItemEditForm = function (itemEl, item) {
	var form = itemEditTemplate.clone();

	itemEl.find('.name, .amount').addClass('hidden')

	var cancelEdit = function () {
		form.remove();
	    itemEl.find('.name.hidden, .amount.hidden').removeClass('hidden');
	}

	var keyupHandle = function (e) {
		if(e.keyCode == 27){
			cancelEdit();
			$(document).unbind("keyup", keyupHandle);
	    }
	}

	$(document).keyup(keyupHandle);

	form.find('.actions .cancel').click(function () {
		cancelEdit();
	});

	itemEl.append(form);

	form.find('[name="name"]').val(item.getName()).focus();
	form.find('[name="amount"]').val(parseInt(item.getAmount()) / 100).numeric();

	onSubmitExpensesItemForm(form).done(function (itemData) {
		item.setProps(itemData);
		cancelEdit();
	});
}

var onActionEditTitle = function (titleEl, list) {
	var deferred = $.Deferred();
	var form = titleEditTemplate.clone();

	var titleField = form.find('[name="title"]').val(list.getTitle());

	titleEl.addClass('hidden');

	var cancelEdit = function () {
		form.remove();
	    titleEl.removeClass('hidden');
	}

	var keyupHandle = function (e) {
		if(e.keyCode == 27){
			cancelEdit();
			$(document).unbind("keyup", keyupHandle);
	    }
	}

	$(document).keyup(keyupHandle);

	titleField.on('keyup change', function () {
		if (!titleField.parent().hasClass('has-error') && !titleField.val()) {
			titleField.parent().addClass('has-error');
			return;
		}
	});

	form.submit(function () {
		form.find('.has-error').removeClass('has-error');
		var hasError = false;

		if (titleField.parent().hasClass('has-error') || !titleField.val()) {
			titleField.parent().addClass('has-error');
			hasError = true;
		}

		if (!hasError) {
			var newTitle = titleField.val();

			list.setTitle(newTitle);

			titleField.val('');

			deferred.resolve(newTitle);

			cancelEdit();
		}

		return false;
	});

	form.insertAfter(titleEl);
	titleField.focus();

	return deferred;
}

var onSubmitExpensesItemForm = function (form) {
	var deferred = $.Deferred();

	var nameField = form.find('[name="name"]');
	var amountField = form.find('[name="amount"]');

	nameField.on('keyup change', function () {
		if (!nameField.parent().hasClass('has-error') && !nameField.val()) {
			nameField.parent().addClass('has-error');
			return;
		}
	});

	amountField.on('keyup change', function (e) {
		var hasError = false;
		var value = amountField.val();

		if (amountField.parent().hasClass('has-error')) {
			hasError = true;
		} else if (!value) {
			hasError = true;
		}

		if (hasError) {
			amountField.parent().addClass('has-error');
			return;
		}
	});

	amountField.numeric();

	form.submit(function () {
		form.find('.has-error').removeClass('has-error');
		var hasError = false;

		if (nameField.parent().hasClass('has-error') || !nameField.val()) {
			nameField.parent().addClass('has-error');
			hasError = true;
		}

		if (amountField.parent().hasClass('has-error') || !amountField.val()) {
			amountField.parent().addClass('has-error');
			hasError = true;
		}

		if (!hasError) {
			var amountStr = amountField.val();
			if (amountStr.indexOf('.') > 0) {
				
				if (amountStr.indexOf('.') > amountStr.length - 3) {
					var diff = amountStr.indexOf('.') - (amountStr.length - 3);
					while (diff > 0) { amountStr += '0'; diff--; }
				} else if (amountStr.indexOf('.') < amountStr.length - 3) {
					var diff = amountStr.length - 3 - amountStr.indexOf('.');
					amountStr = amountStr.slice(diff * -1);
				}

				amountStr = amountStr.replace('.', '');
			}  else {
				amountStr += '00';
			}

			deferred.resolve({name: nameField.val(), amount: parseInt(amountStr)});

			nameField.val('');
			amountField.val('');
		}

		return false;
	});

	return deferred;
}

var ExpensesItemView = function (options) {
	options = options || {};

	var _item = options.item;
	var _containerSelector = options.containerSelector;
	var _containerEl = options.containerEl;

	if (!(_item instanceof ExpensesItem)) {
		_item = new ExpensesItem(_item);
	}

	var _itemEl = _itemTemplate.clone();

	_itemEl.find('.actions .delete').click(function () {
		_item.remove();
		_itemEl.remove();
	});

	_itemEl.find('.actions .edit').click(function () {
		if (_itemEl.find('.name').hasClass('hidden')) {
			return;
		}

		renderItemEditForm(_itemEl, _item);
	});

	// this render method should render only values that can be changed, static values, such as html structure should be rendered only once
	this.render = function () {
		_itemEl.find('.name .value').text(_item.getName());
		_itemEl.find('.amount .value').text(parseInt(_item.getAmount()) / 100);

		return _itemEl;
	}

	// expected jQuery wrapped DOM Node
	if (_containerEl && _containerEl.length > 0) {
		_containerEl.append(this.render());
	} else if (_containerSelector) {
		var selectedNode = $(_containerSelector);

		if (selectedNode.length > 0) {
			selectedNode.append(this.render());
		}
	}
}

var ExpensesListView = function (options) {
	options = options || {};
	var _currentComparator = options.sort || 'name-asc';

	var _list = options.list || new ExpensesList();

	if (!(_list instanceof ExpensesList)) {
		_list = new ExpensesList(_list);
	}

	var _id = _list.getId();
	var _templateSelector = options.templateSelector || '';
	var _containerSelector = options.containerSelector || '';

	var self = this;
	_list.on('change', function () {
		self.render();
	});

	var listComponent = $(_templateSelector).clone();
	var emptyList = listComponent.find('.empty-list');

	var listId = 'expenses-list-' + _id;
	var containerId = 'expenses-container-' + _id;

	listComponent.attr('id', listId).removeClass('hidden');
	listComponent.find('.panel-title a')
		.attr('data-parent', '#' + listId)
		.attr('href', '#' + containerId);
	listComponent.find('.panel-collapse').attr('id', containerId);
	listComponent.find('#name').attr('id', 'name-' + _id);
	listComponent.find('[for="name"]').attr('for', 'name-' + _id);
	listComponent.find('#amount').attr('id', 'amount-' + _id);
	listComponent.find('#options-dropdown').attr('id', 'options-dropdown-' + _id);
	listComponent.find('[aria-labelledby="options-dropdown"]').attr('aria-labelledby', 'options-dropdown-' + _id);
	listComponent.find('[for="amount"]').attr('for', 'amount-' + _id);

	var form = listComponent.find('form');

	onSubmitExpensesItemForm(form).done(function (itemData) {
		_list.addItem(itemData);
	});

	listComponent.find('.remove-list-action').click(function () {
		_list.remove();
		listComponent.remove();
	});

	this.render = function() {
		if (_list.size() == 0) {
			emptyList.removeClass('hidden');
		} else {
			emptyList.addClass('hidden');
		}

		_list.setComparator(_list.getCurrentComparator(), {silent: true})

		if (_list.getCurrentComparator())
			listComponent.attr('sorted', _list.getCurrentComparator());

		//render title
		$('.panel-title .value', listComponent).text(_list.getTitle());

		//render list
		var listItemsContainer = $('.panel-body .list', listComponent).empty();
		var listItems = _list.getItems();

		$.each(_list.getItems(), function (i, listItem) {
			var renderedItem = new ExpensesItemView({item: listItem, containerEl: listItemsContainer});
		});			

		// render total
		listComponent.find('.results .amount .value').text(_list.getTotal() / 100);
	}

	this.getEl = function () {
		return listComponent;
	}

	this.serialize = function () {
		return _list.serialize();
	}

	this.getList = function () {
		return _list;
	}

	listComponent.find('.panel-title a .fa-pencil').click(function () {
		onActionEditTitle(listComponent.find('.panel-title a'), _list);
		return false;
	});

	listComponent.find('.headings a.name').click(function () {
		if (listComponent.attr('sorted') === 'name-asc') {
			_list.setComparator('name-desc', {silent: true}); // using silent to prevent doubled sorting
		} else {
			_list.setComparator('name-asc', {silent: true});
		}

		self.render();
	});

	listComponent.find('.headings a.amount').click(function () {
		if (listComponent.attr('sorted') === 'amount-asc') {
			_list.setComparator('amount-desc', {silent: true});
		} else {
			_list.setComparator('amount-asc', {silent: true});
		}

		self.render();
	});

	$(_containerSelector).append(listComponent);
	listComponent.data('list', _list);
	this.render();
}

var LocalStorageWrapper = function () {
	var storage = window.localStorage;

	this.get = function (key) {
		var value = storage.getItem(key);

        if (typeof value != 'string' || value.length == 0) {
            return {};
        }

        try {
            return JSON.parse(value);
        } catch (err) {
            console.error('Error on parsing record form storage', err);
            return {};
        }
	}

	this.hasKey = function (key) {
		return storage.getItem(key) ? true : false;
	}

	this.save = function (key, data) {
		if (!key) {
            console.error('parameter "key" is required for method "save"');
            return false;
        }

        if (data == undefined) {
            console.error('parameter "data" is required for method "save"');
            return false;
        }

        var valueToSave = data;

        if (typeof valueToSave == 'object') {
            valueToSave = JSON.stringify(valueToSave);
        } else if (typeof valueToSave == 'function') {
            console.error('parameter "data" has to be string, object of array, not a function');
            return false;
        }
        try {
            storage.setItem(key, valueToSave);

            return true;
        } catch (err) {
            console.warn('error on saving item to localStorage', err);
            return false;
        }
	}
}

var ExpensesManager = function (options) {
	options = options || {};

	var templateSelector = options.templateSelector || '#sampleList';
	var containerSelector = options.containerSelector || '#lists';
	var storage = new LocalStorageWrapper();

	var lists = [];
	var renderedLists = [];
	var renderedListsMap = {};

	if (window.localStorage) {
		var expenserData = window.localStorage['expenser'];
	}

	var serialize = function () {
		var serializedLists = [];

		$.each(renderedLists, function (i, list) {
			serializedLists.push(list.serialize());
		});

		return serializedLists;
	}

	var removeList = function (list) {
		for (var i = 0; i < lists.length; i++) {
			if (lists[i].getId() === list.getId()) {
				lists.splice(i, 1);
				break;
			}
		}
		for (i = 0; i < renderedLists.length; i++) {
			if (renderedLists[i].getList().getId() === list.getId()) {
				renderedLists.splice(i, 1);
				break;
			}
		}

		if (renderedListsMap[list.getId()]) {
			delete renderedListsMap[list.getId()];
		}

		if (lists.length > 0) {
			$('#no-lists').removeClass('hidden');
		}

		saveExpenser();
	}

	var saveExpenser = function () {
		$('body').addClass('working');
		storage.save('expenser', serialize());
		$('body').removeClass('working');
	}

	var deserializeLists = function (serializedLists) {
		var deserializedLists = [];

		$.each(serializedLists, function (i, list) {
			deserializedLists.push(new ExpensesList(list));
		});

		return deserializedLists;
	}

	this.loadFromStorage = function () {
		var serializedLists = storage.get('expenser');

		lists = deserializeLists(serializedLists);
	}

	var renderList = function (list) {
		var listView = new ExpensesListView({
			templateSelector: templateSelector,
			containerSelector: containerSelector,
			list: list,
		});

		list = listView.getList(); // this instance is definitely ExpensesItem

		list.on('change', function (e, data) {
			saveExpenser();
		});

		list.on('remove', function (e, list) {
			removeList(list);
		});

		lists.push(list);

		$('#no-lists:not(.hidden)').addClass('hidden');

		renderedLists.push(listView);
		renderedListsMap[list.getId()] = listView;

		saveExpenser();
	}

	this.displayLists = function () {
		if (lists.length == 0) {
			$('#no-lists').removeClass('hidden');
		} else {
			$.each(lists, function (i, list) {
				renderList(list);
			});
		}
	}

	if (storage.hasKey('expenser')) {
		this.loadFromStorage();
	} else {
		saveExpenser();
	}

	$('.add-new-list-action').click(function () {
		var el = $(this);

		var list = new ExpensesList();

		renderList(list);
	});
}

var manager = new ExpensesManager({
	templateSelector: '#sampleList',
	containerSelector: '#lists'
});

manager.displayLists();


